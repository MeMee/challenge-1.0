using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerScript : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    public float verticalInput;
   
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        verticalInput = Input.GetAxis("Vertical");
        
        transform.Rotate(Vector3.right, verticalInput * rotationSpeed * Time.deltaTime);
        
        
    }
}
